export class HashMap<T> {[key: string]: T
}

export class Tuple<T, U> {
  constructor(public left: T, public right: U) { }
}

export interface JsonPropertyMetadata {
  name: any;
  type?: any;
  toJson?: (value: any) => string;
  fromJson?: (value: string) => any;
  context?: string [];
}

export interface JsonPropertiesMetadata {
  names: string[];
  toJsons: ((value: any) => string)[];
  fromJson: (value: string[]) => any;
  context?: string [];
}

export interface JsonOverrideMetadata {
  includes?: string[];
  excludes?: string[];
  context?: string[];
}

export class JsonMapping {
  subType: string;
  property: string;
  context: string [];

  constructor(key: string, context: string []) {
    this.property = key;
    this.context = context || [];
  }

  setSubType = (type: string) => {
    this.subType = type;
    return this;
  };
}

export class JsonPropertyMapping extends JsonMapping {
  name: any;
  type: any;
  toJson: (value: any) => string;
  fromJson: (value: string) => any;

  constructor(key: string, metadata: JsonPropertyMetadata) {
    super(key, metadata.context);

    this.name = metadata.name;
    this.type = metadata.type;
    this.toJson = metadata.toJson;
    this.fromJson = metadata.fromJson;
  }
}

export class JsonPropertiesMapping extends JsonMapping {
  names: string[];
  toJsons: ((value: any) => string)[];
  fromJson: (value: string[]) => any;

  constructor(key: string, metadata: JsonPropertiesMetadata) {
    super(key, metadata.context);

    this.names = metadata.names;
    this.toJsons = metadata.toJsons;
    this.fromJson = metadata.fromJson;
  }
}

export abstract class JsonGenericMapping extends JsonPropertyMapping {
  abstract toGeneric: (value: any, toJson: (v: any) => any) => any;
  abstract fromGeneric: (value: any, fromJson: (v: string) => any) => any;
}

export class JsonArrayMapping extends JsonGenericMapping {
  toGeneric = (value: any[], toJson: (v: any) => any[]) => value == null ? null : value.map(v => toJson(v));
  fromGeneric = (value: any[], fromJson: (v: string) => any[]) => value == null ? null : value.map(v => fromJson(v));

  constructor(key: string, metadata: JsonPropertyMetadata) {
    super(key, metadata);
  }
}

export class JsonMapMapping extends JsonGenericMapping {
  map = (value: any, fn: Function) => Object.keys(value).map(k => new Tuple(k, fn(value[k]))).reduce((r, t) => {
    r[t.left] = t.right;
    return r;
  }, {});

  toGeneric = (value: HashMap<any>, toJson: (v: any) => HashMap<any>) => value == null ? null : this.map(value, toJson);
  fromGeneric = (value: HashMap<any>, fromJson: (v: string) => HashMap<any>) => value == null ? null : this.map(value, fromJson);

  constructor(key: string, metadata: JsonPropertyMetadata) {
    super(key, metadata);
  }
}

export class JsonSubTypeMetadata {
  subType: string;
  type: any;

  constructor(value: string, type: any) {
    this.subType = value;
    this.type = type;
  }
}
