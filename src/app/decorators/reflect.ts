import {JsonMapping, JsonOverrideMetadata, JsonSubTypeMetadata} from './metadata';

export const JSON_MAPPING_METADATA = 'json:mappings';
export const JSON_OVERRIDE_METADATA = 'json:overrides';
export const JSON_TYPE_METADATA = 'json:type';
export const JSON_SUBTYPES_METADATA = 'json:subtypes';
export const JSON_TYPE_PLACEHOLDER = 'head';

export function defineMapping(target: any, metadata: JsonMapping) {
  const reflected = Reflect.hasMetadata(JSON_MAPPING_METADATA, target) ? Reflect.getMetadata(JSON_MAPPING_METADATA, target) : [];
  console.log('set json mapping on target', target, reflected, metadata);
  reflected.push(metadata);
  Reflect.defineMetadata(JSON_MAPPING_METADATA, reflected, target);
}

export function defineOverride(target: any, key: string, metadata: JsonOverrideMetadata) {
  const reflected = Reflect.hasMetadata(JSON_OVERRIDE_METADATA, target, key) ? Reflect.getMetadata(JSON_OVERRIDE_METADATA, target, key) : [];
  console.log('set json override on target property', target, reflected, metadata);
  reflected.push(metadata);
  Reflect.defineMetadata(JSON_OVERRIDE_METADATA, reflected, target, key);
}

export function defineType(target: any, property: string) {
  // updateSubTypeMapping(target, JSON_TYPE_PLACEHOLDER);
  console.log('set json type property on target', target, property);
  Reflect.defineMetadata(JSON_TYPE_METADATA, property, target);
}

export function defineSubtype(target: any, metadata: JsonSubTypeMetadata) {
  const reflected = Reflect.hasMetadata(JSON_SUBTYPES_METADATA, target) ? Reflect.getMetadata(JSON_SUBTYPES_METADATA, target) : [];
  // updateSubTypeMapping(target, metadata.subType);
  console.log('set json subtype on target property', target, reflected, metadata);
  reflected.push(metadata);
  Reflect.defineMetadata(JSON_SUBTYPES_METADATA, reflected, target);
}

function updateSubTypeMapping(target: any, subType: string) {
  const reflected = Reflect.hasMetadata(JSON_MAPPING_METADATA, target) ? Reflect.getMetadata(JSON_MAPPING_METADATA, target) : [];
  console.log('update json mapping on target', target, subType, reflected);
  Reflect.defineMetadata(JSON_MAPPING_METADATA + ':' + subType, reflected, target);
  Reflect.deleteMetadata(JSON_MAPPING_METADATA , target);
}
