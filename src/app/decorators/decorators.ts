import {
  JsonPropertyMetadata, JsonPropertiesMetadata, JsonOverrideMetadata, JsonPropertyMapping, JsonPropertiesMapping,
  JsonArrayMapping, JsonMapMapping, JsonSubTypeMetadata
} from './metadata';
import {defineMapping, defineOverride, defineType, defineSubtype} from './reflect';

export function JsonProperty(metadata: JsonPropertyMetadata) {
  return (target: any, key: string) => {
    defineMapping(target, new JsonPropertyMapping(key, metadata));
  };
}

export function JsonProperties(metadata: JsonPropertiesMetadata) {
  return (target: any, key: string) => {
    defineMapping(target, new JsonPropertiesMapping(key, metadata));
  };
}

export function JsonArray(metadata: JsonPropertyMetadata) {
  return (target: any, key: string) => {
    defineMapping(target, new JsonArrayMapping(key, metadata));
  };
}

export function JsonMap(metadata: JsonPropertyMetadata) {
  return (target: any, key: string) => {
    defineMapping(target, new JsonMapMapping(key, metadata));
  };
}

export function JsonOverride(metadata: JsonOverrideMetadata) {
  return (target: any, key: string) => {
    defineOverride(target, key, metadata);
  };
}

export function JsonType(name: string) {
  return (constructor: Function) => {
    defineType(constructor.prototype, name);
  };
}

export function JsonSubType(value: string) {
  return (constructor: Function) => {
    defineSubtype(constructor.prototype, new JsonSubTypeMetadata(value, constructor));
  };
}
