import {HashMap} from './metadata';
import {JsonProperties, JsonProperty, JsonMap, JsonArray, JsonType, JsonSubType} from './decorators';

export class Included {

  @JsonProperties({names: ['fooA', 'fooB'], toJsons: [(v) => v + 'A', (v) => v + 'B'], fromJson: (vs) => vs.join('')})
  foo: string;

  @JsonProperty({name: 'barC'})
  bar: number;
}

@JsonType('type')
export class Parent {

  @JsonProperty({name: 'barC', type: Included})
  foo: Included;

  @JsonArray({name: 'barC', type: Number})
  bar: number[];

  @JsonMap({name: 'barC', type: Boolean})
  zoo: HashMap<boolean>;
}

@JsonSubType('A')
export class ChildA extends Parent {

  type: string = 'A';

  @JsonProperty({name: 'fooCA'})
  fooCA: string;

  @JsonProperty({name: 'barCA'})
  zarCA: number;
}

@JsonSubType('B')
export class ChildB extends Parent {

  type: string = 'B';

  @JsonProperty({name: 'fooCB'})
  fooCB: string;

  @JsonProperty({name: 'barCB'})
  zarCB: number;
}
