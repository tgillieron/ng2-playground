import {BehaviorSubject, Observable} from 'rxjs';
/**
 * Created by thomasgillieron on 19/10/2016.
 */

export function bindTemplate(template: string, values: Object) {
  return template.replace(/\${(.*?)}/g, (match: string, p1: string) => values[p1]);
}

export class HttpEvent {
  id: string;
  url: string;
}

export class HttpRequestEvent extends HttpEvent {
}

export class HttpResponseEvent extends HttpEvent {
}

export class InlineUrls {
  id: string;
  urls: string[];
}

export enum loaderState { IDLE, URL, ACTION, INLINE_URL, INLINE_ACTION}

export class LoaderRef {

  private urlBuffer: string[] = [];
  status: string = '';

  constructor(private state: loaderState, private inlineUrls: InlineUrls, private loader: Loader, httpEvents: Observable<HttpEvent>) {
    if (inlineUrls) {
      loader.registerInlineUrl(inlineUrls);
    }

    if (state === loaderState.INLINE_URL) {
      httpEvents.subscribe((event: HttpEvent) => {
        if (event instanceof HttpRequestEvent && !this.isInline(event.url)) {
          this.urlBuffer.push(event.id);
        } else if (event instanceof HttpResponseEvent) {
          this.urlBuffer = this.urlBuffer.filter((id: string) => id !== event.id);
        }

        // TODO timeout
        this.status = this.urlBuffer.length > 0 ? 'pending' : '';
      });
    }
  }

  start() {
    if (this.state === loaderState.INLINE_ACTION) {
      this.status = 'pending'; // TODO timeout
    }
    if (this.state === loaderState.ACTION) {
      this.loader.updateStatus(this.state);
    }
  }

  done() {
    if (this.state === loaderState.INLINE_ACTION) {
      this.status = ''; // TODO timeout
    }
    if (this.state === loaderState.ACTION) {
      this.loader.updateStatus(this.state);
    }
  }

  ok() {
    if (this.state === loaderState.INLINE_ACTION) {
      this.status = 'promise-ok'; // TODO timeout
    }
    if (this.state === loaderState.ACTION) {
      this.loader.updateStatus(this.state);
    }
  }

  ko() {
    if (this.state === loaderState.INLINE_ACTION) {
      this.status = 'promise-ko'; // TODO timeout
    }
    if (this.state === loaderState.ACTION) {
      this.loader.updateStatus(this.state);
    }
  }

  destroy() {
    if (this.inlineUrls) {
      this.loader.removeInlineUrl(this.inlineUrls.id);
    }
  }

  private isInline(url): boolean {
    return this.inlineUrls.urls.filter(u => url.indexOf(u) !== -1).length === 0;
  }
}

export class Loader {
  private urlBuffer: string[] = [];
  private inlineUrls: InlineUrls[] = [];
  stateSubject: BehaviorSubject<loaderState> = new BehaviorSubject<loaderState>(loaderState.IDLE);

  constructor(httpEvents: Observable<HttpEvent>) {
    httpEvents.subscribe((event: HttpEvent) => {
      if (event instanceof HttpRequestEvent && !this.isInline(event.url)) {
        this.urlBuffer.push(event.id);
      } else if (event instanceof HttpResponseEvent) {
        this.urlBuffer = this.urlBuffer.filter((id: string) => id !== event.id);
      }

      this.updateStatus(loaderState.URL);
    });
  }

  registerInlineUrl(inlines: InlineUrls) { this.inlineUrls.push(inlines); }

  removeInlineUrl(id: string) { this.inlineUrls = this.inlineUrls.filter((inline: InlineUrls) => id !== inline.id); }

  // TODO navigation sucess = idle <= put in loader
  updateStatus(state: loaderState) {
    let newState = this.stateSubject.getValue();
    if (state === loaderState.URL) {
      newState = newState === loaderState.ACTION ? loaderState.ACTION : this.urlBuffer.length > 0 ? loaderState.URL : loaderState.IDLE;
    }

    if (state === loaderState.ACTION) {
      newState = newState !== loaderState.ACTION ? loaderState.ACTION : this.urlBuffer.length > 0 ? loaderState.URL : loaderState.IDLE;
    }

    if (state === loaderState.IDLE) {
      newState = this.urlBuffer.length > 0 ? loaderState.URL : loaderState.IDLE;
    }

    if (newState !== this.stateSubject.getValue()) {
      this.stateSubject.next(newState);
    }
  }

  private isInline(url): boolean {
    return this.inlineUrls.reduce((r, i) => r.concat(i.urls), []).filter(u => url.indexOf(u) !== -1).length === 0;
  }
}

export class LoaderService {

  private loader: Loader;

  constructor(private http: any) {
    this.loader = new Loader(http.events);
  }

  getStatus() { return this.loader.stateSubject.asObservable(); }

  getLoaderFor(state: loaderState, inlineUrls?: InlineUrls) { return new LoaderRef(state, inlineUrls, this.loader, this.http.events); }
}
