import {Component} from '@angular/core';
import {Input} from '@angular/core/src/metadata/directives';

@Component({
  selector: 'pg-demo',
  template: `
    <div class="demo">
        <h1>{{title}}</h1>
        <div class="example"><ng-content></ng-content></div>
    </div>
  `,
  styles: [`
    .demo {
        margin: 30px;
        border-radius: 5px;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }
    
    .demo h1 {
        margin: 0;
        padding-left: 10px;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
        background-color: #b6b6b6;
        color: black;
        text-transform: capitalize;
    }
    
    .example {
        padding: 10px;
    }
  `]
})
export class DemoComponent {
  @Input() title: string;
}
