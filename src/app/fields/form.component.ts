import {Component, ContentChildren, QueryList} from '@angular/core';
import {FFieldsetComponent} from './fieldset.component';
import {Observable, Subscription} from 'rxjs';
import 'rxjs/add/operator/merge';
import 'rxjs/add/operator/distinctUntilChanged';

@Component({
  selector: 'f-form',
  template: `<form><ng-content></ng-content></form>`
})
export class FFormComponent {

  private _touched: Subscription;
  private _fieldsets: FFieldsetComponent[];

  set touched(touched: Subscription) {
    if (this._touched != null) { this._touched.unsubscribe(); }
    this._touched = touched;
  }

  @ContentChildren(FFieldsetComponent) set fieldsets(fieldsets: QueryList<FFieldsetComponent>) {
    this._fieldsets = fieldsets.toArray();
    this.touched = this._fieldsets.length === 0 ? null : Observable.merge(...(this._fieldsets.map(f => f.touchedEvents())))
                                                                   .distinctUntilChanged()
                                                                   .subscribe(f => this._fieldsets
                                                                                       .slice(0, this._fieldsets.indexOf(f))
                                                                                       .forEach(ff => ff.activate()));
  };
}
