import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {NgModule} from '@angular/core';
import {FFormComponent} from './form.component';
import {FFieldsetComponent} from './fieldset.component';
import {FFInputComponent, FFSelectComponent, FFButtonComponent, FFRegisterDirective} from './field.component';
import {FFieldsDemoComponent} from './fields-demo.component';


const COMPONENTS = [
  FFormComponent,
  FFieldsetComponent,
  FFInputComponent,
  FFSelectComponent,
  FFieldsDemoComponent,
  FFButtonComponent,
  FFRegisterDirective
];

@NgModule({
  imports: [CommonModule, FormsModule],
  declarations: [...COMPONENTS],
  exports: [...COMPONENTS],
  providers: [],
})
export class FieldsModule {
}
