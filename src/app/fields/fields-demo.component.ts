import {Component} from '@angular/core';
@Component({
  selector: 'fields-demo',
  template: `
    <f-form>
        <fieldset fTitle="f1">
            <ff-input></ff-input>
        </fieldset>
        <fieldset fTitle="f2">
            <ff-input></ff-input>
            <ff-select></ff-select>
            <button ffButton ffRegister></button>
        </fieldset>
        <fieldset fTitle="f3">
            <ff-input></ff-input>
            <ff-input></ff-input>
            <ff-select></ff-select>
        </fieldset>
    </f-form>
  `
})
export class FFieldsDemoComponent {
}
