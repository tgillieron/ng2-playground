import {Component, Input} from '@angular/core';
import {FFieldComponent} from './field.component';
import {Subject} from 'rxjs';
@Component({
  selector: 'fieldset[fTitle]',
  template: `<legend>{{title}}</legend><ng-content></ng-content>`
})
export class FFieldsetComponent {

  private fields: FFieldComponent[] = [];
  private toucheEvent: Subject<FFieldsetComponent> = new Subject<FFieldsetComponent>();

  @Input('fTitle') title: string;

  register(field: FFieldComponent) { this.fields.push(field); }

  remove(field: FFieldComponent) { this.fields = this.fields.filter(f => f !== field); }

  touched() { this.toucheEvent.next(this); }

  touchedEvents() { return this.toucheEvent.asObservable(); }

  activate() { this.fields.forEach(f => f.activate()); }
}
