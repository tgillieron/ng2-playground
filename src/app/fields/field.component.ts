import {FFieldsetComponent} from './fieldset.component';
import {OnDestroy, Component, Directive, HostListener} from '@angular/core';

export class FFieldComponent implements OnDestroy {

  activated: boolean;

  constructor(private fieldset: FFieldsetComponent) { fieldset.register(this); }

  touched() { this.fieldset.touched(); }

  activate() { this.activated = true; }

  ngOnDestroy() { this.fieldset.remove(this); }
}

@Component({
  selector: 'ff-input',
  template: `<label>Input</label><input (focus)="touched()"/><span *ngIf="activated">ACTIVATED</span>`
})
export class FFInputComponent extends FFieldComponent {

  constructor(fieldset: FFieldsetComponent) { super(fieldset); }
}

@Component({
  selector: 'ff-select',
  template: `<label>Input</label><select (focus)="touched()"></select><span *ngIf="activated">ACTIVATED</span>`
})
export class FFSelectComponent extends FFieldComponent {

  constructor(fieldset: FFieldsetComponent) { super(fieldset); }
}

@Component({
  selector: 'button[ffButton]',
  template: `TEST`
})
export class FFButtonComponent {
}

@Directive({selector: '[ffRegister]'})
export class FFRegisterDirective extends FFieldComponent {

  constructor(private fieldseto: FFieldsetComponent) { super(fieldseto); }

  @HostListener('focus') touched() { this.fieldseto.touched(); }
}
