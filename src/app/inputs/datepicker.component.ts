import {Component, Input, forwardRef, ElementRef, Renderer} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {DatePicker} from './datepicker';

export const F_DATEPICKER_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => FDatepickerComponent),
  multi: true
};

@Component({
  selector: 'f-datepicker',
  template: `
    <input 
        type="text" [value]="value" [disabled]="disabled" [name]="name" [placeholder]="placeholder" [attr.size]="size" 
        (blur)="onBlur()" (focus)="onFocus($event)" (input)="onInput($event)" (keydown)="onKeypress($event)"
    />
    <div *ngIf="open" class="picker" tabindex="-1"
        [style.top]="_position?.top" [style.left]="_position?.left" 
        (focus)="onFocus($event)">
        <div>
            <button (blur)="onBlur()" (focus)="onFocus($event)" (click)="previous()"> < </button>
            {{picker.header}}
            <button (blur)="onBlur()" (focus)="onFocus($event)" (click)="next()"> > </button></div>
        <table>
            <tr>
                <td  *ngFor="let day of picker.weekDays">{{day}}</td>
            </tr>
            <tr *ngFor="let week of picker.days">
                <td *ngFor="let day of week" 
                    [class.today]="picker.isToday(day)" [class.input]="picker.isInput(day)" 
                    (click)="onClick(day)">{{day}}</td>
            </tr>
        </table>
    </div>
  `,
  styles: [`
    .picker {
        width: 230px;
        max-height: 180px;
        border: 1px solid black;
        background-color: white;
        position: absolute;
        z-index: 1;
        display: block;
    }
    
    .today {
        background-color: red;
    }
    
    .input {
        background-color: blue;
    }
  `],
  providers: [F_DATEPICKER_VALUE_ACCESSOR]
})
export class FDatepickerComponent implements ControlValueAccessor {

  private _open: boolean = false;

  private _position: {top: string, left: string};

  private _blurTimeout: any;

  private picker: DatePicker = new DatePicker();

  private _onModelChange = (_: any) => _;

  private _onModelTouched = () => {};

  @Input() name: string;

  @Input() size: number;

  @Input() placeholder: string = '';

  @Input() disabled: boolean;

  @Input() value: string;

  constructor(private _renderer: Renderer, private _elementRef: ElementRef) { }

  get open(): boolean { return !this.disabled && this._open; }

  previous() {
    this.picker.previous();
    this._renderer.invokeElementMethod(this._elementRef.nativeElement.children[0], 'focus');
  }

  next() {
    this.picker.next();
    this._renderer.invokeElementMethod(this._elementRef.nativeElement.children[0], 'focus');
  }

  updateValue(newValue: string, oldValue: string) {
    if (newValue !== oldValue) {
      this._onModelChange(newValue);
    }
    this.value = newValue;
  }

  onFocus() {
    clearTimeout(this._blurTimeout);
    // TODO offset position
    const elementPosition = (<HTMLInputElement>this._elementRef.nativeElement).getBoundingClientRect();
    this._position = {top: `${elementPosition.bottom}px`, left: `${elementPosition.left}px`};
    this._open = true;
  }

  onBlur() {
    this._blurTimeout = setTimeout(() => this._open = false, 250);
    this._onModelTouched();
  }

  onInput(event: Event) {
    this.picker.inputDate((<HTMLInputElement>event.target).value);
    this.updateValue((<HTMLInputElement>event.target).value, this.value);
  }

  onKeypress(event: KeyboardEvent) {
    const key = event.key;

    if (key === 'Escape') {
      event.preventDefault();
      this._open = false;
    }

    if (key === 'Enter' && this.open) {
      event.preventDefault();
      this._open = false;
      if (this.value == null || this.value === '') {
        this.updateValue(this.picker.todayString(), this.value);
      }
    }
  }

  onClick(day: string) {
    if (day !== '') {
      this.updateValue(this.picker.date(day), this.value);
      this.picker.inputDate(this.picker.date(day));
      // TODO Should we focus on input ? this._renderer.invokeElementMethod(this._elementRef.nativeElement.children[0], 'focus');
      this._open = false;
    }
  }

  writeValue(value: any): void {
    this.picker.inputDate(value);
    this.value = value;
  }

  registerOnChange(fn: (_: number) => void): void { this._onModelChange = fn; }

  registerOnTouched(fn: () => void): void { this._onModelTouched = fn; }

  setDisabledState(isDisabled: boolean): void { this.disabled = isDisabled; }

}
