import {Component, Input, forwardRef} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

export const F_TEXTAREA_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => FTextareaComponent),
  multi: true
};

@Component({
  selector: 'f-textarea',
  template: `
    <div>
        <div class="backdrop">
            <div class="highlights" [innerHTML]="marks">
            </div>
        </div>
        <textarea [value]="value" [disabled]="disabled" (input)="onInput($event)" (scroll)="onScroll($event)"></textarea>
    </div>
  `,
  styles: [`
    .container {
        display: block;
        margin: 0 auto;
    }
    
    .backdrop {
        position: absolute;
        z-index: 1;
        overflow: auto;
        pointer-events: none;
    }

    .highlights {
        padding: 10px;
        color: transparent;
        white-space: pre-wrap;
        word-wrap: break-word;
        font: 16px 'Open Sans', sans-serif;
        letter-spacing: 1px;
    }
    
    mark {
        border-radius: 3px;
        color: transparent;
        background-color: #b1d5e5;
    }
    
    textarea {
        z-index: 2;
        margin: 0;
        padding: 10px;
        border-radius: 0;
        background-color: transparent;
        font: 16px 'Open Sans', sans-serif;
        letter-spacing: 1px;
    }
  `],
  providers: [F_TEXTAREA_VALUE_ACCESSOR]
})
export class FTextareaComponent implements ControlValueAccessor {

  private _onModelChange = (_: any) => void 0;

  private _onModelTouched = () => {};

  private marks: string = '';

  @Input() value: any = '';

  @Input() disabled: boolean;

  onInput(event: Event) {
    const value =  (<HTMLInputElement>event.target).value;
    this.marks = value.split(' ').map( s => `<mark>${s}</mark>`).join(' ');
  }

  onScroll(event: Event) {
    // TODO $backdrop.scrollTop($textarea.scrollTop());
    // TODO var scrollLeft = this.$el.scrollLeft();
    // this.$backdrop.css('transform', (scrollLeft > 0) ? 'translateX(' + -scrollLeft + 'px)' : '');
  }

  writeValue(value: any): void { this.value = value; }

  registerOnChange(fn: (_: number) => void): void { this._onModelChange = fn; }

  registerOnTouched(fn: () => void): void { this._onModelTouched = fn; }

  setDisabledState(isDisabled: boolean): void { this.disabled = isDisabled; }

}

// TODO highlight all words
// TODO highlight only selected words
