import {Component, Input, ContentChildren, QueryList, forwardRef} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {FOptionDirective} from './option.directive';

export const F_AUTOCOMPLETE_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => FAutocompleteComponent),
  multi: true
};

@Component({
  selector: 'f-autocomplete',
  template: `
    <input 
        type="text" autocomplete="off" 
        [value]="_predicat" [disabled]="disabled" [name]="name" [placeholder]="placeholder" [attr.size]="size" 
        (blur)="onBlur()" (input)="onInput($event)" (keydown)="onKeypress($event)"
    />
    <ul *ngIf="open" class="select-box">
        <li *ngFor="let option of _choices" 
            [class.selected]="option.selected" 
            (click)="onClick(option)">{{option.label}}</li>
    </ul>
    <ng-content select="f-option"></ng-content>
  `,
  styles: [`
    .select-box {
        width: 230px;
        border: 1px solid black;
        list-style-type: none;
        margin: 0;
        padding: 0;
    }
    
    .selected {
        background-color: blue;
    }
  `],
  providers: [F_AUTOCOMPLETE_VALUE_ACCESSOR]
})
export class FAutocompleteComponent implements ControlValueAccessor {

  @Input() name: string;

  @Input() size: number;

  @Input() placeholder: string = '';

  @Input() disabled: boolean;

  private _predicat: string = '';

  private _value: any;

  private _options: FOptionDirective[] = [];

  private _choices: FOptionDirective[] = [];

  private _open: boolean = false;

  private _onModelChange = (_: any) => void 0;

  private _onModelTouched = () => {};

  @Input() set value(value: string) {
    const choice = this._options.filter(o => o.value === this._value);
    this.select(choice[0]);
    this._value = value;
    this._predicat = choice.length > 0 ? choice[0].label : '';
  }

  @ContentChildren(FOptionDirective) set options(options: QueryList<FOptionDirective>) {
    this._options = options.toArray();
    this._predicat = '';
    this.updateValue(null, this._value);
  }

  get open(): boolean { return !this.disabled && this._open; }

  updateValue(newValue: string, oldValue: string) {
    if (newValue !== oldValue) {
      this._onModelChange(newValue);
    }
    this._value = newValue;
  }

  select(option: FOptionDirective) { this._options.forEach(o => o.selected = option != null && o.value === option.value); }

  selectedIndex(): number { return this._choices.reduce((r, o, i) => o.selected ? i : r, -1); }

  onInput(event: Event) {
    this._predicat = (<HTMLInputElement>event.target).value;
    this._choices = this._options.filter(o => o.label.toUpperCase().indexOf(this._predicat.toUpperCase()) !== -1);
    this._open = this._choices.length > 0;
    this.updateValue(null, this._value);
  }

  onBlur() {
    this._onModelTouched();
  }

  onKeypress(event: KeyboardEvent) {
    const key = event.key;

    if (key === 'Escape') {
      event.preventDefault();
      this._open = false;
    }

    if (key === 'Enter' && this.open && this.selectedIndex() > 0) {
      event.preventDefault();
      this.onClick(this._choices[this.selectedIndex()]);
    }

    if (key === 'ArrowDown') {
      if (!this.open && this._choices.length > 0) {
        event.preventDefault();
        this._open = true;
      } else if (this._open) {
        event.preventDefault();
        const index = this.selectedIndex() + 1;
        this.select(this._choices[index > this._choices.length ? 0 : index]);
      }
    }

    if (key === 'ArrowUp') {
      if (!this.open && this._choices.length > 0) {
        event.preventDefault();
        this._open = true;
      } else if (this._open) {
        event.preventDefault();
        const index = this.selectedIndex() - 1;
        this.select(this._choices[index < -1 ? this._choices.length - 1 : index]);
      }
    }
  }

  onClick(option: FOptionDirective) {
    this.select(option);
    this._predicat = option.label;
    this._open = false;
    this.updateValue(option.value, this._value);
  }

  writeValue(value: any): void { this.value = value; }

  registerOnChange(fn: (_: number) => void): void { this._onModelChange = fn; }

  registerOnTouched(fn: () => void): void { this._onModelTouched = fn; }

  setDisabledState(isDisabled: boolean): void { this.disabled = isDisabled; }

}
