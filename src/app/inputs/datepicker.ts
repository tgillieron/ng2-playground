const EMPTY_DAYS = '                                          '.split('');
const DAYS = ['Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam', 'Dim'];
const MONTHS = ['Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Decembre'];

export class DatePicker {

  private matchDate = /^[0-3][0-9]\/[0-1][0-9]\/[0-9]{4}$/;

  weekDays = DAYS;

  today = new Date();

  input = new Date();

  current = new Date();

  days: string[][];

  constructor() {
    this.today.setHours(0, 0, 0, 0);
    this.current.setDate(1);
    this.days = this.buildDays();
  }

  get header(): string { return `${MONTHS[this.current.getMonth()]} ${this.current.getFullYear()}`; }

  date(day: string): string {
    const month = this.current.getMonth() + 1;
    return `${+day > 9 ? day : ('0' + day)}/${month > 9 ? month : ('0' + month)}/${this.current.getFullYear()}`;
  }

  todayString(): string {
    const month = this.today.getMonth() + 1;
    const day = this.today.getDay() + 1;
    return `${+day > 9 ? day : ('0' + day)}/${month > 9 ? month : ('0' + month)}/${this.today.getFullYear()}`;
  }

  inputDate(date: string) {
    const input = date != null ? date.split('/').reverse().map(s => +s) : [];
    this.input = new Date(input[0], input[1] - 1, input[2]);

    const valid = this.matchDate.test(date) && !isNaN(this.input.getTime());
    const notCurrent = this.current.getFullYear() !== input[0] || this.current.getMonth() !== input[1] - 1;

    if (valid && notCurrent) {
      this.current.setFullYear(input[0], input[1] - 1);
      this.days = this.buildDays();
    }
  }

  previous() {
    const month = this.current.getMonth() - 1;
    if (month < 0) {
      this.current.setFullYear(this.current.getFullYear() - 1, 11);
    } else {
      this.current.setMonth(month);
    }

    this.days = this.buildDays();
  }

  next() {
    const month = this.current.getMonth() + 1;
    if (month > 11) {
      this.current.setFullYear(this.current.getFullYear() + 1, 0);
    } else {
      this.current.setMonth(month);
    }

    this.days = this.buildDays();
  }

  isToday(day: string): boolean {
    return this.today.getTime() === new Date(this.current.getFullYear(), this.current.getMonth(), +day).getTime();
  }

  isInput(day: string): boolean {
    return this.input.getTime() === new Date(this.current.getFullYear(), this.current.getMonth(), +day).getTime();
  }

  private buildDays(): string[][] {
    const dayOfTheWeek = this.current.getDay() - 1;
    const daysInMonth = new Date(this.current.getFullYear(), this.current.getMonth() + 1, 0).getDate() - 1;
    const days = EMPTY_DAYS.map((d, i) => i < dayOfTheWeek || i > (daysInMonth + dayOfTheWeek) ? '' : (i + 1 - dayOfTheWeek).toString());

    return [
      days.slice(0, 7),
      days.slice(7, 14),
      days.slice(14, 21),
      days.slice(21, 28),
      days.slice(28, 35),
      days.slice(35),
    ];
  }
}
