import {Component, Input, ContentChildren, QueryList, forwardRef, AfterContentInit} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {FOptionDirective} from './option.directive';

export const F_SELECT_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => FSelectComponent),
  multi: true
};

@Component({
  selector: 'f-select',
  template: `
    <p class="select" tabindex="0" (blur)="onBlur()" (click)="onInput()" (keydown)="onKeypress($event)">{{_predicate}}</p>
    <ul *ngIf="open" class="select-box" >
        <li *ngFor="let option of _options" 
            [class.selected]="option.selected" 
            (click)="onClick(option)">{{option.label}}</li>
    </ul>
    <ng-content select="f-option"></ng-content>
  `,
  styles: [`
    .select {
        height: 16px;
        width: 125px;
        padding: 1px;
        border: 2px solid rgb(238, 238, 238);
    }
    .select:focus {
        box-shadow: 0 0 5px rgba(81, 203, 238, 1);
        border: 1px solid rgba(81, 203, 238, 1);
    }
    
    .select-box {
        width: 230px;
        border: 1px solid black;
        list-style-type: none;
        margin: 0;
        padding: 0;
    }
    
    .selected {
        background-color: blue;
    }
  `],
  providers: [F_SELECT_VALUE_ACCESSOR]
})
export class FSelectComponent implements ControlValueAccessor, AfterContentInit {

  @Input() disabled: boolean;

  /**
   * displayed selected label
   * @type {string}
   * @private
   */
  private _predicate: string = '';

  /**
   * selected value
   * @type {any}
   * @private
   */
  private _value: any;
  private _init: any;

  /**
   * list of available options
   * @type {Array}
   * @private
   */
  private _options: FOptionDirective[] = [];

  /**
   * should the choices be displayed
   * @type {boolean}
   * @private
   */
  private _open: boolean = false;

  /**
   * ngModel placeholder
   * @param _
   * @private
   */
  private _onModelChange = (_: any) => {};

  /**
   * ngModel placeholder
   * @private
   */
  private _onModelTouched = () => {};

  /**
   * set the value
   * if the value is available in one option it will select it and update the predicate
   * @param value
   */
  @Input() set value(value: string) {
    const choice = this._options.filter(o => o.value === this._value);
    this.select(choice[0]);
    this._value = value;
    this._predicate = choice.length > 0 ? choice[0].label : '';
  }

  /**
   * set the list of options
   * it will also clear the selected options, predicate and value
   * @param options
   */
  @ContentChildren(FOptionDirective) set options(options: QueryList<FOptionDirective>) {
    this._options = options.toArray();
    this._predicate = '';
    this._init = this._value;
    this.updateValue(null, this._value);
  }

  /**
   * are the choices displayed
   * true if the element is not disabled and sould be displayed
   * @returns {boolean}
   */
  get open(): boolean { return !this.disabled && this._open; }

  /**
   * update the value
   * if the new value is different from the old send the value to the ngModel
   * @param newValue
   * @param oldValue
   */
  updateValue(newValue: string, oldValue: string) {
    if (newValue !== oldValue) {
      this._onModelChange(newValue);
    }
    this._value = newValue;
  }

  /**
   * select the option in the list of choices
   * if the option is null or is not available it reset the selection
   * @param option
   */
  select(option: FOptionDirective) { this._options.forEach(o => o.selected = option != null && o.value === option.value); }

  /**
   * get the index of the selected option. -1 if no option is selected
   * @returns {number}
   */
  selectedIndex(): number { return this._options.reduce((r, o, i) => o.selected ? i : r, -1); }

  /**
   * on blur event close the display and send the information to the ngModel
   */
  onBlur() {
    this._onModelTouched();
  }

  /**
   * on input event open the display
   */
  onInput() { this._open = true; }

  /**
   * on keypress event
   * - Escape: close the display
   * - Enter: commit the selected value then close the display
   * - ArrowUp/Down: open the display and selecte the next choice
   * @param event
   */
  onKeypress(event: KeyboardEvent) {
    const key = event.key;

    if (key === 'Escape') {
      event.preventDefault();
      this._open = false;
    }

    if (key === 'Enter' && this.open && this.selectedIndex() > 0) {
      event.preventDefault();
      this.onClick(this._options[this.selectedIndex()]);
    }

    if (key === 'ArrowDown') {
      event.preventDefault();

      if (!this.open) {
        this._open = true;
      } else {
        const index = this.selectedIndex() + 1;
        this.select(this._options[index >= this._options.length ? 0 : index]);
      }
    }

    if (key === 'ArrowUp') {
      event.preventDefault();

      if (!this.open) {
        this._open = true;
      } else {
        const index = this.selectedIndex() - 1;
        this.select(this._options[index < 0 ? this._options.length - 1 : index]);
      }
    }
  }

  /**
   * on click on a choice update the selected option, predicate and the value then close the display
   * @param option
   */
  onClick(option: FOptionDirective) {
    this.select(option);
    this._predicate = option.label;
    this._open = false;
    this.updateValue(option.value, this._value);
  }

  /**
   * on component init force the fist value
   */
  ngAfterContentInit(): void { this.value = this._init; } // TODO

  /**
   * ngModel placeholder
   * @param value
   */
  writeValue(value: any): void { this.value = value; }

  /**
   * ngModel placeholder
   * @param fn
   */
  registerOnChange(fn: (_: number) => void): void { this._onModelChange = fn; }

  /**
   * ngModel placeholder
   * @param fn
   */
  registerOnTouched(fn: () => void): void { this._onModelTouched = fn; }

  /**
   * ngModel placeholder
   * @param isDisabled
   */
  setDisabledState(isDisabled: boolean): void { this.disabled = isDisabled; }

}
