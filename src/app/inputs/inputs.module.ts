import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {NgModule} from '@angular/core';
import {FOptionDirective} from './option.directive';
import {FAutocompleteComponent} from './autocomplete.component';
import {FSelectComponent} from './select.component';
import {FDatepickerComponent} from './datepicker.component';
import {FTextareaComponent} from './textarea.component';

const INPUTS = [
  FOptionDirective,
  FAutocompleteComponent,
  FSelectComponent,
  FDatepickerComponent,
  FTextareaComponent
];

@NgModule({
  imports: [CommonModule, FormsModule],
  declarations: [...INPUTS],
  exports: [...INPUTS],
  providers: [],
})
export class InputsModule {
}
