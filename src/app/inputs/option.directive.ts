import {Directive, Input} from '@angular/core';

@Directive({selector: 'f-option'})
export class FOptionDirective {
  @Input() label: string;
  @Input() value: string;

  selected: boolean = false;
}
