import {Component} from '@angular/core';
import {Parent, ChildA, ChildB} from './decorators/decorator-demo';
import {JSON_MAPPING_METADATA, JSON_SUBTYPES_METADATA, JSON_TYPE_METADATA} from './decorators/reflect';
import {bindTemplate} from './utils/utils';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-root',
  template: `
    <h1>{{title}}</h1>
   
    <pg-demo title="Autocomplete">
        <f-autocomplete [ngModel]="auto" (ngModelChange)="ngModelChange($event, 'autocomplete')">
            <f-option value="A" label="AAA"></f-option>
            <f-option value="B" label="AAB"></f-option>
            <f-option value="C" label="ACA"></f-option>
            <f-option value="D" label="DAA"></f-option>
        </f-autocomplete>
    </pg-demo>
    
    <pg-demo title="Select">
        <f-select value="A" (ngModelChange)="ngModelChange($event, 'select')">
            <f-option value="A" label="AAA"></f-option>
            <f-option value="B" label="AAB"></f-option>
            <f-option value="C" label="ACA"></f-option>
            <f-option value="D" label="DAA"></f-option>
        </f-select>
    </pg-demo>
    
    <pg-demo title="Datepicker">
        <f-datepicker [ngModel]="picket" (ngModelChange)="ngModelChange($event, 'datepicker')"></f-datepicker>
    </pg-demo>
    
    <pg-demo title="Highlight">
        <f-textarea></f-textarea>
    </pg-demo>
    
    <pg-demo title="Dialog"></pg-demo>
    
    <fields-demo></fields-demo>
  `
})
export class AppComponent {
  title = 'ng2-playgroud!';

  ngModelChange(change: string, from: string) { console.log(`ngModelChange from ${from} : `, change); }

  constructor() { }

}
