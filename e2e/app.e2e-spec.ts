import { Ng2PlaygroudPage } from './app.po';

describe('ng2-playgroud App', function() {
  let page: Ng2PlaygroudPage;

  beforeEach(() => {
    page = new Ng2PlaygroudPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
